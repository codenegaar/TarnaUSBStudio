#ifndef USBMANAGERLINUXIMPL_H
#define USBMANAGERLINUXIMPL_H

#include "usbmanager.h"

class UsbManagerLinuxImpl : public UsbManager
{
public:
    UsbManagerLinuxImpl();
    
    QVector<QString> getUsbPaths() override;
};

#endif // USBMANAGERLINUXIMPL_H
