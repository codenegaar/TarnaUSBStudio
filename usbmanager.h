#ifndef USBMANAGER_H
#define USBMANAGER_H

#include <QString>
#include <QVector>

class UsbManager
{
public:
    static UsbManager& instance();
    virtual ~UsbManager();
    
    virtual QVector<QString> getUsbPaths() = 0;
    virtual bool formatUsb(QString usbPath) = 0;
};

#endif // USBMANAGER_H
